<?php

namespace BoxPay\Gateway\PagarMe;

use BoxPay\Entities\Client;
use BoxPay\Gateway\AbstractGateway;
use BoxPay\Gateway\Exception;
use BoxPay\Operation\Billet;
use BoxPay\Operation\CreditCard;

class PagarMeGateway extends AbstractGateway implements CreditCard, Billet
{

    const PAYMENT_METHOD_CREDIT_CARD = 'credit_card';
    const PAYMENT_METHOD_BILLET      = 'boleto';

    protected $postbackUrl;
    protected $softDescriptor;
    protected $async;

    public function __construct($postbackUrl, $softDescriptor, $async = false)
    {
        $this->postbackUrl    = $postbackUrl;
        $this->softDescriptor = $softDescriptor;
        $this->async          = $async;
    }

    public function createCreditCardPayment($reference, $amount, Client $client, \BoxPay\Entities\CreditCard $creditCard)
    {
        try {

            $data = [];

            if ($creditCard->hasCardId()) {
                $data['card_id'] = $creditCard->getCardId();
            } else {

                $card = new \PagarMe_Card();
                $card->card_number           = $creditCard->getNumber();
                $card->card_holder_name      = $creditCard->getHolderName();
                $card->card_expiration_month = $creditCard->getExpirationMonth();
                $card->card_expiration_year  = $creditCard->getExpirationYear();
                $card->card_cvv              = $creditCard->getSecurityCode();

                $data['card_hash'] = $card->generateCardHash();

            }

            $transaction = $this->createTransaction(
                $reference,
                $amount,
                self::PAYMENT_METHOD_CREDIT_CARD,
                $data,
                $client
            );

            return $this->parseResponse(new ResponseParser($transaction));

        } catch (\PagarMe_Exception $e) {
            throw new Exception('Error on create credit card payment: ' . $e->getMessage());
        }
    }

    public function createBilletPayment($reference, $amount, Client $client, \DateTime $expirationDate = null)
    {
        try {

            $data = [];

            if ($expirationDate) {
                $data['boleto_expiration_date'] = $expirationDate->format(\DateTime::ISO8601);
            }

            $transaction = $this->createTransaction(
                $reference,
                $amount,
                self::PAYMENT_METHOD_BILLET,
                $data,
                $client
            );

            return $this->parseResponse(new ResponseParser($transaction));

        } catch (\PagarMe_Exception $e) {
            throw new Exception('Error on create billet payment: ' . $e->getMessage());
        }
    }

    protected function createTransaction($reference, $amount, $paymentMethod, array $methodData, Client $client)
    {
        $data = [
            'amount'          => $amount,
            'payment_method'  => $paymentMethod,
            'postback_url'    => $this->postbackUrl,
            'async'           => $this->async ? 'true' : false,
            'soft_descriptor' => $this->softDescriptor,
            'metadata'        => [
                'reference' => $reference
            ],
        ];

        if ($client) {
            $data['customer'] = [
                'name' => $client->getName(),
                'document_number' => $client->getDocumentNumber(),
                'email' => $client->getEmail(),
            ];
        }

        $data = array_merge($data, $methodData);

        $transaction = new \PagarMe_Transaction($data);
        $transaction->charge();

        return $transaction;
    }

}