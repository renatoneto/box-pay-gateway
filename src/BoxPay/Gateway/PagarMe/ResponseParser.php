<?php

namespace BoxPay\Gateway\PagarMe;

use BoxPay\Entities\Billet;
use BoxPay\Entities\CreditCard;
use BoxPay\Gateway\Response;

class ResponseParser implements \BoxPay\Gateway\ResponseParser
{

    protected $transaction;

    protected $response;

    public function __construct(\PagarMe_Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->response    = new Response();
    }

    /**
     * @return Response
     */
    public function parse()
    {
        $this->response->setTransactionId($this->transaction->id);
        $this->response->setStatus($this->transaction->status);

        switch ($this->transaction->payment_method) {

            case PagarMeGateway::PAYMENT_METHOD_BILLET:

                $billet = new Billet();
                $billet->setUrl($this->transaction->boleto_url);
                $billet->setBarcode($this->transaction->boleto_barcode);

                $this->response->setBillet($billet);

                break;

            case PagarMeGateway::PAYMENT_METHOD_CREDIT_CARD:

                $creditCard = new CreditCard();

                $creditCard->setHolderName($this->transaction->card->holder_name);
                $creditCard->setNumber(
                    $this->transaction->card->first_digits . '******' . $this->transaction->card->last_digits
                );

                $creditCard->setBrand(ucfirst(
                    $this->transaction->card->brand
                ));

                $creditCard->setCardId($this->transaction->card->id);

                $this->response->setCreditCard($creditCard);

                $this->response->setAcquirerName($this->transaction->acquirer_name);
                $this->response->setAcquirerResponseCode($this->transaction->acquirer_response_code);
                $this->response->setTid($this->transaction->tid);
                $this->response->setPaidAmount($this->transaction->paid_amount);

                break;

        }

        $reflection = new \ReflectionClass($this->transaction);
        $attributes = $reflection->getProperty('_attributes');
        $attributes->setAccessible(true);

        $this->response->setCompleteAttributes($attributes->getValue($this->transaction));

        return $this->response;
    }

}