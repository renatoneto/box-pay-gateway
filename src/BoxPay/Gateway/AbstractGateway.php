<?php

namespace BoxPay\Gateway;

abstract class AbstractGateway
{

    public function parseResponse(ResponseParser $parser)
    {
        return $parser->parse();
    }

}