<?php

namespace BoxPay\Gateway;

use BoxPay\Entities\Billet;
use BoxPay\Entities\CreditCard;

class Response
{

    public $status;
    public $transactionId;

    protected $billet;
    protected $creditCard;

    protected $completeAttributes;

    protected $paidAmount;
    protected $acquirerName;
    protected $acquirerResponseCode;
    protected $tid;

    public function getBillet()
    {
        return $this->billet;
    }

    public function setBillet(Billet $billet)
    {
        $this->billet = $billet;
    }

    public function getCreditCard()
    {
        return $this->creditCard;
    }

    public function setCreditCard(CreditCard $creditCard)
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getCompleteAttributes()
    {
        return $this->completeAttributes;
    }

    /**
     * @param mixed $completeAttributes
     */
    public function setCompleteAttributes($completeAttributes)
    {
        $this->completeAttributes = $completeAttributes;
    }

    /**
     * @return mixed
     */
    public function getPaidAmount()
    {
        return $this->paidAmount;
    }

    /**
     * @param mixed $paidAmount
     */
    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    }

    /**
     * @return mixed
     */
    public function getAcquirerName()
    {
        return $this->acquirerName;
    }

    /**
     * @param mixed $acquirerName
     */
    public function setAcquirerName($acquirerName)
    {
        $this->acquirerName = $acquirerName;
    }

    /**
     * @return mixed
     */
    public function getAcquirerResponseCode()
    {
        return $this->acquirerResponseCode;
    }

    /**
     * @param mixed $acquirerResponseCode
     */
    public function setAcquirerResponseCode($acquirerResponseCode)
    {
        $this->acquirerResponseCode = $acquirerResponseCode;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * @param mixed $tid
     */
    public function setTid($tid)
    {
        $this->tid = $tid;
    }

}