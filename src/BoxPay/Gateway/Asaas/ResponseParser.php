<?php

namespace BoxPay\Gateway\Asaas;

use BoxPay\Entities\Billet;
use BoxPay\Entities\Transaction;
use BoxPay\Gateway\Response;
use Softr\Asaas\Entity\Payment;

class ResponseParser implements \BoxPay\Gateway\ResponseParser
{

    protected $asaasPayment;

    protected $response;

    public function __construct(Payment $asaasPayment)
    {
        $this->asaasPayment = $asaasPayment;
        $this->response = new Response();
    }

    /**
     * @return Response
     */
    public function parse()
    {
        $this->response->setTransactionId($this->asaasPayment->id);

        $this->response->setStatus(Transaction::STATUS_WAITING_PAYMENT);

        $billet = new Billet();
        $billet->setUrl($this->asaasPayment->boletoUrl);
        $billet->setBarcode($this->asaasPayment->invoiceNumber);

        $this->response->setBillet($billet);

        $this->response->setCompleteAttributes($this->asaasPayment);

        return $this->response;
    }

}