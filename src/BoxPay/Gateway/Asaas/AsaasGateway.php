<?php

namespace BoxPay\Gateway\Asaas;

use BoxPay\Entities\Client;
use BoxPay\Gateway\AbstractGateway;
use BoxPay\Gateway\Exception;
use BoxPay\Operation\Billet;
use Softr\Asaas\Asaas;

class AsaasGateway extends AbstractGateway implements Billet
{

    protected $asaasClient;
    protected $dueDate;
    protected $description;

    public function __construct(Asaas $asaas, \DateTime $dueDate, $description, $async = false)
    {
        $this->asaasClient = $asaas;
        $this->dueDate     = $dueDate;
        $this->description = $description;
    }

    public function createBilletPayment($reference, $amount, Client $client, \DateTime $expirationDate = null)
    {
        try {

            $asaasCustomer = $this->asaasClient->customer()->getByEmail($client->getEmail());

            if (empty($asaasCustomer)) {

                $customerData = [
                    'name'    => $client->getName(),
                    'email'   => $client->getEmail(),
                    'cpfCnpj' => $client->getDocumentNumber(),
                ];

                $asaasCustomer = $this->asaasClient->customer()->create($customerData);

                if (!$asaasCustomer) {
                    throw new Exception('Error on create billet customer: ' . json_encode($customerData));
                }

            }

            $paymentEntity = $this->asaasClient->payment()->create([
                'customer'    => $asaasCustomer->id,
                'value'       => $amount / 100,
                'description' => $this->description,
                'billingType' => 'BOLETO',
                'dueDate'     => $this->dueDate->format('d/m/Y'),
            ]);

            return $this->parseResponse(new ResponseParser($paymentEntity));

        } catch (\Exception $e) {
            throw new Exception('Error on create billet payment: ' . $e->getMessage());
        }
    }

}