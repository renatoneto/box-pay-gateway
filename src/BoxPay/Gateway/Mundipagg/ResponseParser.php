<?php

namespace BoxPay\Gateway\Mundipagg;

use BoxPay\Entities\CreditCard;
use BoxPay\Entities\Transaction;
use BoxPay\Gateway\Response;
use Gateway\One\DataContract\Response\BaseResponse;

class ResponseParser implements \BoxPay\Gateway\ResponseParser
{

    protected $mundipaggResponse;

    protected $response;

    public function __construct(BaseResponse $mundipaggResponse)
    {
        $this->mundipaggResponse = $mundipaggResponse;
        $this->response = new Response();
    }

    /**
     * @return Response
     */
    public function parse()
    {
        $responseData = $this->mundipaggResponse->getData();

        if (!empty($responseData->OrderResult->OrderKey)) {
            $this->response->setTransactionId($responseData->OrderResult->OrderKey);
        }

        if ($this->mundipaggResponse->isSuccess()) {
            $this->response->setStatus(Transaction::STATUS_PAID);
        } else {
            $this->response->setStatus(Transaction::STATUS_REFUSED);
        }

        $creditCard = new CreditCard();
        $creditcardResult = $responseData->CreditCardTransactionResultCollection[0];

        $creditCard->setHolderName('');
        $creditCard->setNumber(
            $creditcardResult->CreditCard->MaskedCreditCardNumber
        );

        $creditCard->setBrand($creditcardResult->CreditCard->CreditCardBrand);

        $creditCard->setCardId($creditcardResult->CreditCard->InstantBuyKey);

        $this->response->setCreditCard($creditCard);


        $this->response->setAcquirerName($creditcardResult->AcquirerName);
        $this->response->setAcquirerResponseCode($creditcardResult->AcquirerReturnCode);
        $this->response->setTid($creditcardResult->TransactionKeyToAcquirer);
        $this->response->setPaidAmount($creditcardResult->CapturedAmountInCents);

        $this->response->setCompleteAttributes($responseData);

        return $this->response;
    }

}