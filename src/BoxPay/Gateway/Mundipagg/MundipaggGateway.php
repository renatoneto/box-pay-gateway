<?php

namespace BoxPay\Gateway\Mundipagg;

use BoxPay\Entities\Client;
use BoxPay\Gateway\AbstractGateway;
use BoxPay\Gateway\Exception;
use BoxPay\Operation\CreditCard;
use Gateway\ApiClient;
use Gateway\One\DataContract\Enum\CreditCardOperationEnum;
use Gateway\One\DataContract\Enum\PaymentMethodEnum;
use Gateway\One\DataContract\Report\ApiError;
use Gateway\One\DataContract\Report\CreditCardError;
use Gateway\One\DataContract\Request\CreateSaleRequest;
use Gateway\One\Helper\CreditCardHelper;

class MundipaggGateway extends AbstractGateway implements CreditCard
{

    /**
     * @var ApiClient
     */
    protected $apiClient;

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function createCreditCardPayment($reference, $amount, Client $client, \BoxPay\Entities\CreditCard $creditCard)
    {
        try {

            $createSaleRequest = new CreateSaleRequest();

            $creditCardTransaction = $createSaleRequest->addCreditCardTransaction()
                ->setAmountInCents($amount)
                ->setPaymentMethodCode(PaymentMethodEnum::AUTO)
                ->setCreditCardOperation(CreditCardOperationEnum::AUTH_AND_CAPTURE)
                ->getCreditCard()
                ->setSecurityCode($creditCard->getSecurityCode());

            if ($creditCard->hasCardId()) {
                $creditCardTransaction->setInstantBuyKey($creditCard->getCardId());
            } else {

                $creditCardTransaction->setCreditCardBrand(CreditCardHelper::getBrandByNumber($creditCard->getNumber()))
                    ->setCreditCardNumber($creditCard->getNumber())
                    ->setExpMonth($creditCard->getExpirationMonth())
                    ->setExpYear($creditCard->getExpirationYear())
                    ->setHolderName($creditCard->getHolderName());

            }

            $createSaleRequest->getOrder()->setOrderReference($reference);

            $response = $this->apiClient->createSale($createSaleRequest);

            return $this->parseResponse(new ResponseParser($response));

        } catch (CreditCardError $error) {
            throw new Exception('CreditCardError on create credit card payment: ' . $error->getMessage());
        } catch (ApiError $error) {
            throw new Exception('ApiError on create credit card payment: ' . $error->getMessage());
        } catch (\Exception $ex) {
            throw new Exception('Error on create credit card payment: ' . $ex->getMessage());
        }

    }

}