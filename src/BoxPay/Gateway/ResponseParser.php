<?php

namespace BoxPay\Gateway;

interface ResponseParser
{

    public function parse();

}