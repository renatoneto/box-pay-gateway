<?php

namespace BoxPay\Gateway;

use BoxPay\Entities\Client;
use BoxPay\Operation\Billet;
use BoxPay\Operation\CreditCard;

class BoxPay implements Billet, CreditCard
{

    /**
     * @var Billet
     */
    protected $billetGateway;

    /**
     * @var CreditCard
     */
    protected $creditCardGateway;

    /**
     * @return Billet
     */
    public function getBilletGateway()
    {
        return $this->billetGateway;
    }

    /**
     * @param Billet $billetGateway
     */
    public function setBilletGateway(Billet $billetGateway)
    {
        $this->billetGateway = $billetGateway;
    }

    /**
     * @return CreditCard
     */
    public function getCreditCardGateway()
    {
        return $this->creditCardGateway;
    }

    /**
     * @return bool
     */
    public function canCreateBilletPayment()
    {
        return $this->billetGateway !== null;
    }

    /**
     * @param CreditCard $creditCardGateway
     */
    public function setCreditCardGateway(CreditCard $creditCardGateway)
    {
        $this->creditCardGateway = $creditCardGateway;
    }

    /**
     * @return bool
     */
    public function canCreateCreditCardPayment()
    {
        return $this->creditCardGateway !== null;
    }

    public function createBilletPayment($reference, $amount, Client $client, \DateTime $expirationDate = null)
    {
        if (!$this->canCreateBilletPayment()) {
            throw new Exception('Has no billet gateway configured');
        }

        return $this->billetGateway->createBilletPayment($reference, $amount, $client, $expirationDate);
    }

    public function createCreditCardPayment($reference, $amount, Client $client, \BoxPay\Entities\CreditCard $creditCard)
    {
        if (!$this->canCreateCreditCardPayment()) {
            throw new Exception('Has no credit card gateway configured');
        }

        return $this->creditCardGateway->createCreditCardPayment($reference, $amount, $client, $creditCard);
    }

}