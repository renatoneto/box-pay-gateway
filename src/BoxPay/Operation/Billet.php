<?php

namespace BoxPay\Operation;

use BoxPay\Entities\Client;

interface Billet
{

    public function createBilletPayment($reference, $amount, Client $client, \DateTime $expirationDate = null);

}