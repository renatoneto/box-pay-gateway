<?php

namespace BoxPay\Operation;

use BoxPay\Entities\Client;

interface CreditCard
{

    public function createCreditCardPayment($reference, $amount, Client $client, \BoxPay\Entities\CreditCard $creditCard);

}