<?php

namespace BoxPay\Entities;

class Transaction
{

    const STATUS_PROCESSING      = 'processing';
    const STATUS_AUTHORIZED      = 'authorized';
    const STATUS_PAID            = 'paid';
    const STATUS_REFUNDED        = 'refunded';
    const STATUS_WAITING_PAYMENT = 'waiting_payment';
    const STATUS_PENDING_REFUND  = 'pending_refund';
    const STATUS_REFUSED         = 'refused';
    const STATUS_CHARGEDBACK     = 'chargedback';

}