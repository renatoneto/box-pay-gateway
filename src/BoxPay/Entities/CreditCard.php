<?php

namespace BoxPay\Entities;

class CreditCard
{

    protected $number;

    protected $expirationMonth;

    protected $expirationYear;

    protected $holderName;

    protected $securityCode;

    protected $cardId;

    protected $brand;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $number = trim($number);
        $number = str_replace(['.'], null, $number);

        $length = strlen($number);

        if ($length < 14 || $length > 16) {
            throw new \BadMethodCallException('Invalid credit card number');
        }

        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getExpirationMonth()
    {
        return $this->expirationMonth;
    }

    /**
     * @param mixed $expirationMonth
     */
    public function setExpirationMonth($expirationMonth)
    {
        $expirationMonth = trim($expirationMonth);

        if ($expirationMonth < 1 || $expirationMonth > 12) {
            throw new \BadMethodCallException('Invalid credit card expiration month');
        }

        $this->expirationMonth = str_pad($expirationMonth, 2, '0', STR_PAD_LEFT);
    }

    /**
     * @return mixed
     */
    public function getExpirationYear()
    {
        return $this->expirationYear;
    }

    /**
     * @param mixed $expirationYear
     */
    public function setExpirationYear($expirationYear)
    {
        $expirationYear = trim($expirationYear);

        if (strlen($expirationYear) != 2) {
            throw new \BadMethodCallException('Invalid credit card expiration year');
        }

        $this->expirationYear = $expirationYear;
    }

    /**
     * @return mixed
     */
    public function getHolderName()
    {
        return $this->holderName;
    }

    /**
     * @param mixed $holderName
     */
    public function setHolderName($holderName)
    {
        $this->holderName = trim($holderName);
    }

    /**
     * @return mixed
     */
    public function getSecurityCode()
    {
        return $this->securityCode;
    }

    /**
     * @param mixed $securityCode
     */
    public function setSecurityCode($securityCode)
    {
        $securityCode = trim($securityCode);
        $length       = strlen($securityCode);

        if ($length < 3 || $length > 4) {
            throw new \BadMethodCallException('Invalid credit card security code');
        }

        $this->securityCode = $securityCode;
    }

    /**
     * @return boolean
     */
    public function hasCardId()
    {
        return $this->cardId !== null;
    }

    /**
     * @return mixed
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @param mixed $cardId
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

}